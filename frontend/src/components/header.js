import styles from "../styles/Navbar.module.css";
import Logo from "../image/food.png"
import Cart from "../image/cart.png"
import { useState } from "react";
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import {Link } from "react-router-dom";
import * as React from 'react';

function LinkTab(props) {
    return (
      <Tab
        component="a"
        onClick={(event) => {
          event.preventDefault();
        }}
        {...props}
      />
    );

  }
  export default function Test() {
    const [value, setValue] = useState(0);
  
    const handleChange = (event, newValue) => {
      setValue(newValue);
    };
  
    return(
        <div className={styles.container}>
             <img src={Logo} alt="" width="50" height="50" />
            <div className={styles.text}>
               
                &nbsp; &nbsp; CS Restaurant
            </div>
            <div className={styles.item}>
      <Tabs value={value} onChange={handleChange} aria-label="nav tabs example">
        
        <Tab label="หน้าแรก"  to='/' component={Link}/>
        <Tab label="เมนูอาหาร" to='/menu' component={Link} />
        <Tab label="ติดต่อเรา" to='/contact' component={Link} />
        <Tab label="เกี่ยวกับร้าน" to='/about' component={Link} />
        <Tab label="Log in" to='/login' component={Link} />
      </Tabs>
        
        <div className={styles.cart}>
          <img src={Cart} alt="" width="30px" height="30px" />
          <div className={styles.counter}>0</div>
        </div>
        </div>
        </div>
    );
}

/*
        <Link to="/about">
          <button> ORDER NOW </button>
        </Link>
*/