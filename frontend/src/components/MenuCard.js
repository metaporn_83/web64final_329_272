import "../styles/MenuCard.css";


function MenuCard() {
  return (
    <div className="wrapper">
      <Card
        img="https://img.freepik.com/free-photo/top-view-griled-beef-steak-served-with-vegetables-sauce-wooden-board_140725-12379.jpg?w=1060https://images.unsplash.com/photo-1536304929831-ee1ca9d44906?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjE0NTg5fQ"
        title="สเต็กเนื้อ"
        price="฿100"
      />

      <Card
        img="https://img.freepik.com/free-photo/mixed-sushi-set-japanese-food_1339-3610.jpg?t=st=1648974221~exp=1648974821~hmac=249fda5c8b98b7a66e020b903bc833119cb23da252bff6ce56fdebe0cdfd6ad8&w=1060"
        title="ซูชิ"
        price="฿20"
      />

      <Card
        img="https://img.freepik.com/free-photo/healthy-hearty-salad-tuna-green-beans-tomatoes-eggs-potatoes-black-olives-close-up-bowl-table_2829-4485.jpg?t=st=1648973558~exp=1648974158~hmac=dd2c155f3c45b7b48ebaafc1ae7b3cb306a9429aef1c4f9dc3de51802ab7180e&w=1060"
        title="สลัดผัก"
        price="฿60"
      />

    <Card
        img="https://img.freepik.com/free-photo/crispy-french-fries-with-ketchup-mayonnaise_1150-26588.jpg?t=st=1649039074~exp=1649039674~hmac=e54b8f6042d11ea4054ab250a019d3ecc2c7a06668dfc5da23c4a47208298f8e&w=900"
        title="เฟรนช์ฟรายส์"
        price="฿30"
      />
    <Card
        img="https://img.freepik.com/free-photo/american-shrimp-fried-rice-served-with-chili-fish-sauce-thai-food_1150-26577.jpg?t=st=1649039420~exp=1649040020~hmac=c7aff67c02403c39a10a42720b5357036fbdcc582c66b8bcebf28aa9de6c2f1a&w=900"
        title="ข้าวผัด"
        price="฿60"
      />
    <Card
        img="https://img.freepik.com/free-photo/rice-bowl_1387-181.jpg?t=st=1649039510~exp=1649040110~hmac=2b9a98a2bd22ed471305ff5314c4ada97f3713f572f2ead8266702ae71e52aa2&w=900"
        title="ข้าวสวย"
        price="฿10"
      />
    <Card
        img="https://img.freepik.com/free-photo/big-sandwich-hamburger-burger-with-beef-red-onion-tomato-fried-bacon_2829-5398.jpg?t=st=1649039585~exp=1649040185~hmac=57a68a018721d18aefef93b292d2038ecde010a6ca77210e1ee6227ab62c3eed&w=900"
        title="แฮมเบอร์เกอร์"
        price="฿45"
      />
    <Card
        img="https://img.freepik.com/free-photo/fresh-flavorful-mashed-potatoes_2829-11441.jpg?t=st=1649039683~exp=1649040283~hmac=ab9f3fea27f85c6c4ed2296c84930db39e0fe082c0b28734b4fd630baada24e7&w=900"
        title="มันบด"
        price="฿35"
      />
    <Card
        img="https://img.freepik.com/free-photo/fried-chicken-wings-wooden-table_1205-7734.jpg?t=st=1649039792~exp=1649040392~hmac=cc248c779ef1399cdc766ed8495efa1d54879a8138f39b07dc72cb3b2bb21370&w=900"
        title="ไก่ทอด"
        price="฿40"
      />
    <Card
        img="https://img.freepik.com/free-photo/pizza-pizza-filled-with-tomatoes-salami-olives_140725-1200.jpg?t=st=1649039856~exp=1649040456~hmac=b9661b18f87de1494b5d7496bf4c0214c46df291021b6cec2479e81e1035e283&w=900"
        title="พิซซ่า"
        price="฿199"
      />
    </div>
  );
}

function Card(props) {
  return (
    <div className="Mcard">
      <div className="Mcard-body">
        <img src={props.img} class="Mcard-image" />
        <h1 className="Mcard-title">{props.title}</h1>
        <span className="Mcard-price">{props.price}</span>
      </div>
          <button className="Mcard-btn"> ADD TO CART </button>
    </div>
  );
}


export default MenuCard;
