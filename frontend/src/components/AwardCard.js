import "../styles/Card.css";


function AwardCard() {
  return (
    <div className="wrapper">
      <Card
        img="https://img.wongnai.com/p/1920x0/2018/01/12/b7a4ca1956e44f51bc459374ea172321.jpg"
        title="Wongnai user Choice 2018"
        description="รางวัลร้านที่ยอดเยี่ยม ที่เฟ้นหากลุ่มร้านที่ดีที่สุดจากความเห็นของสมาชิก Wongnai กว่า 3 ล้านคน  "
      />

      <Card
        img="https://cdn.i-scmp.com/sites/default/files/d8/images/canvas/2022/02/02/34a882dd-e79e-4961-888c-5f651850dffe_4b018da2.jpg"
        title="Michelin Bib Gourmand "
        description="รางวัลที่ทางมิชลินไกด์ให้กับ 'ร้านอาหารอร่อยราคาเข้าถึงได้' ราคาไม่เกิน 1,000 บาท สำหรับอาหารหนึ่งมื้อ"
      />

      <Card
        img="https://149362394.v2.pressablecdn.com/wp-content/uploads/2020/02/Michelin-One-Star.png"
        title="1 Michelin star"
        description="รางวัลสำหรับร้านอาหารคุณภาพสูง ที่ควรค่าแก่การหยุดแวะชิม"
        price="฿60"
      />
    </div>
  );
}

function Card(props) {
  return (
    <div className="card">
      <div className="card-body">
        <img src={props.img} class="card-image" />
        <h2 className="card-title">{props.title}</h2>
        <p className="card-description">{props.description}</p>
      </div>
    </div>
  );
}

export default AwardCard;