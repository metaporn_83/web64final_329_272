import "../styles/Card.css";


function RecommandedCard() {
  return (
    <div className="wrapper">
      <Card
        img="https://img.freepik.com/free-photo/top-view-griled-beef-steak-served-with-vegetables-sauce-wooden-board_140725-12379.jpg?w=1060https://images.unsplash.com/photo-1536304929831-ee1ca9d44906?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjE0NTg5fQ"
        title="สเต็กเนื้อ"
        description="สเต็กเนื้อโคขุนคุณภาพ รสชาติอร่อย ราคาถูก ที่ได้รับรางวัลดาวมิชลิน 1 ดาวสามปีซ้อน"
        price="฿100"
      />

      <Card
        img="https://img.freepik.com/free-photo/mixed-sushi-set-japanese-food_1339-3610.jpg?t=st=1648974221~exp=1648974821~hmac=249fda5c8b98b7a66e020b903bc833119cb23da252bff6ce56fdebe0cdfd6ad8&w=1060"
        title="ซูชิ"
        description="ชูชิคำโต อร่อยเต็มคำ จากวัตถุดิบที่มีคุณภาพ  ในราคาประหยัด"
        price="฿20"
      />

      <Card
        img="https://img.freepik.com/free-photo/healthy-hearty-salad-tuna-green-beans-tomatoes-eggs-potatoes-black-olives-close-up-bowl-table_2829-4485.jpg?t=st=1648973558~exp=1648974158~hmac=dd2c155f3c45b7b48ebaafc1ae7b3cb306a9429aef1c4f9dc3de51802ab7180e&w=1060"
        title="สลัดผัก"
        description="สลัดผักสดปลอดสารพิษ และน้ำสลัดโฮมเมด ที่ทางร้านปลูกผักทำฟาร์มเป็นของตัวเอง รับประกันความสดกรอบ ปลอดสารพิษ 100%"
        price="฿60"
      />
    </div>
  );
}

function Card(props) {
  return (
    <div className="card">
      <div className="card-body">
        <img src={props.img} class="card-image" />
        <h2 className="card-title">{props.title}</h2>
        <p className="card-description">{props.description}</p>
      </div>
        <span className="card-price">{props.price}</span>
    </div>
  );
}


export default RecommandedCard;

