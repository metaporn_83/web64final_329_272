import * as React from 'react';
import '../styles/About.css'
import {Box} from '@mui/material'
import AwardCard from './AwardCard';
//import '../image/3.5star.png'

function About(){
    return(
        <Box >
            <h1> ABOUT US</h1>
        
        <div class="container">   
            <img src="https://img.freepik.com/free-photo/hamburgers-with-fries-cup-sauce-wooden-tray_181624-2600.jpg?t=st=1649000268~exp=1649000868~hmac=e3d36a80feabb121b9a04bc10cf8116c4f8927002033a93fc6d56fdcfe240d8f&w=1060" class="container-image" />
                <div class="container-text">
                    <p>  ด้วยการยึดหลัก “สะอาด อร่อย และราคายุติธรรม" และด้วยรสชาติของความอร่อยของ SC Restaurant 
                     จึงมีสื่อต่างๆ หลายแขนง ทั้งทีวี หนังสือพิมพ์ ไม่จะเป็นรายวัน รายสัปดาห์ วารสารนิตยสารตลอดจนหน่วยงานของรัฐและเอกชนอีกไม่น้อย 
                     ที่มักจะเชื้อเชิญ ให้ไปโชว์ว่าความอร่อย รวมทั้งรางวัลที่ทางร้าน ได้รับจากอีกหลายสถาบัน ที่เป็นสิ่งที่ทางร้านได้มาด้วยความมุ่งมั่นอดทน</p>
                </div>
                </div>
            <h1> รางวัลที่เคยได้รับ</h1>  
                <AwardCard />
            <h1> คะแนนรีวิวจากแอปพลิเคชันต่างๆ </h1> <br/> <br/> <br/>
           
            <div class="star-Rate">
            <img src="https://static2.wongnai.com/static2/images/HTZaHLM.png" class="rate" />
            <br/><img src={require('../image/3.5star.png')}  class="star"/>
            </div>

            <div class="star-Rate">
            <img src="https://www.gran-turismo.com/gtsport/decal/7278450323121767448_1.png" class="rate" />
            <br/><img src={require('../image/4.5star.png')}  class="star"/>
            </div>

            <div class="star-Rate">
            <img src="https://cdn.ligor.cloud/spai/w_263+q_lossy+ret_img+to_webp/https://www.farmfactoryworld.com/wp-content/uploads/2020/07/20012021_144738_1050a7d-removebg-preview.png" class="rate" />
            <br/><img src={require('../image/4star.png')}  class="star"/>
            </div>

        </Box>

      
    )
}

export default About; 

