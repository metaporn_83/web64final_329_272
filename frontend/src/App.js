import './App.css';
import React from "react";
import Header from './components/header';
import Footer from './components/Footer';
import Home from './pages/Home'
import AboutUspage from './pages/AboutUspage';
import Contact from './pages/Contact';
import Menu from './pages/Menu';
import LoginPage from './pages/LoginPage';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';


function App() {
  return (
    <div className="App">
        
        <Router>
          <Header/>
          <Routes>  
            <Route path="/" element= {<Home/>} />
            < Route path="menu" element={<Menu/>} />
            < Route path="contact" element={<Contact/>} />
            < Route path="about" element={<AboutUspage/>} />
            < Route path="login" element={<LoginPage/>} />
        </Routes>
        <Footer />
        </Router>
    </div>
  );
}

export default App;


/*
import logo from './logo.svg';
<img src={logo} className="App-logo" alt="logo" />
*/      