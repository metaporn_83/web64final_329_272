import RecommandedCard from "../components/RecommandedCard";
import BannerImage from "../image/MenuPic.jpg";
import MenuCard from "../components/MenuCard";

function Menu() {
    return (
      <div class="menu-body">
      <div className="menu" style={{ backgroundImage: `url(${BannerImage})` }}>
        <div className="headerContainer">
          <h1 style={{ fontWeight: "bold" }}> ลดทันที 20 % </h1>
         <p>หากสั่งอาหารออนไลน์ครั้งแรกวันนี้!!</p>
           <hr/>
        </div>
        </div>
   <div className="Recommanded">
        <MenuCard/>
  </div>
  </div>
    );
  }
  export default Menu;