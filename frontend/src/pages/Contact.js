import React from "react";
import "../styles/Contact.css";
import PizzaLeft from "../image/ContactPic.jpg";
import { Box,TextField,Button,Stack} from "@mui/material";
import { Facebook , Twitter,Instagram,Phone,Send } from "@mui/icons-material";
function Contact() {
    return (
    <Box> 
      <div className="self-contact">
        <div className="leftSide"
          style={{ backgroundImage: `url(${PizzaLeft})` }}
        ></div>
        <div className="rightSide">
          <h1> ติดต่อโดยตรง</h1>
          <Box
            component="form"
            sx={{
                '& > :not(style)': { m: 5, width: '12ch' }, }}
            noValidate
            autoComplete="off"
            >
            <TextField id="outlined-basic" label="ชื่อ-สกุล" variant="outlined" style ={{width: '80%'}}/>
            <TextField id="outlined-basic" label="Email" variant="outlined"style ={{width: '80%'}} />
            <TextField id="outlined-basic" label="ข้อความ" variant="outlined"style ={{width: '80%'}} />
            
            <Button variant="contained" size="large" endIcon={<Send />} style={{justifyContent: 'center'}}>
                    Send
            </Button>
        
        </Box>

      </div>
      </div>
      <div className="contactbottom">
            <div className="socialMedia">
            <h1 className="title-contact">ช่องทางการติดต่อ</h1>
            <Facebook />
            <Instagram/>
            <Twitter/>
            <Phone/>
            </div>
        </div>
     </Box>
    );
  }
  
  export default Contact;