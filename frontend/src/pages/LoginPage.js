import React from "react";
import "../styles/Login.css";
import { Box,TextField,Button,Stack} from "@mui/material";

function LoginPage() {
    return (
    <Box> 
      <div className="login">
        <div className="leftSide">
        <h1> Log in </h1>
            <Box
            component="form"
                    sx={{
                        '& > :not(style)': { m: 2, width: '12ch' }, }}
                    noValidate
                    autoComplete="off"
            >
                    <TextField id="outlined-basic" label="Username" variant="outlined"style ={{width: '80%'}} />
                    <TextField id="outlined-basic" label="Password" variant="outlined"style ={{width: '80%'}} />           
                
                    <Button variant="contained" size="large" style={{width:"300px"}}>
                            Log in
                    </Button>
        
        </Box>
        </div>
        <div className="rightSide">
          <h1> Sign Up</h1>
          <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 2, width: '12ch' }, }}
                    noValidate
                    autoComplete="off"
            >
                    <TextField id="outlined-basic" label="ชื่อ" variant="outlined" style ={{width: '80%'}}/>
                    <TextField id="outlined-basic" label="นามสกุล" variant="outlined" style ={{width: '80%'}}/>
                    <TextField id="outlined-basic" label="Username" variant="outlined"style ={{width: '80%'}} />
                    <TextField id="outlined-basic" label="Password" variant="outlined"style ={{width: '80%'}} />
                    
                    <Button variant="contained" size="large" style={{width:"300px"}}>
                            Sign up
                    </Button>
        
        </Box>
            
      </div>
        </div>
     </Box>
    );
  }
  
  export default LoginPage;