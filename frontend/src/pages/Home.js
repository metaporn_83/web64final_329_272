import React from "react";
import { Link } from "react-router-dom";
import RecommandedCard from "../components/RecommandedCard";
import BannerImage from "../image/HomePic.jpg";
import "../styles/Home.css";

function Home() {
  return (
    <div class="home-body">
    <div className="home" style={{ backgroundImage: `url(${BannerImage})` }}>
      <div className="headerContainer">
        <h1 style={{ fontWeight: "bold" }}> CS Restaurant </h1>
        <p></p>
        <Link to="/menu">
          <button> ORDER NOW </button>
        </Link>
      </div>
      </div>
 <div className="Recommanded">
    <h6 class="Rec-name">CS Restaurant</h6>
    <h1>เมนูแนะนำ</h1>
      <RecommandedCard/>
</div>
</div>
  );
}

export default Home;