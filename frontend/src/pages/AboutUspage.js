import { Restaurant } from "@mui/icons-material";
import React from "react";
import "../styles/About.css";
import RestaurantPic from "../image/RestaurantPic.jpg"
import About from "../components/About";
function AboutUspage() {
  return (
    <div className="about">
       <div
        className="aboutTop"
        style={{ backgroundImage: `url(${RestaurantPic})` }}
      ></div>
      <div className="aboutBottom">
                <About/>
      </div>
    </div>
  );
}

export default AboutUspage;