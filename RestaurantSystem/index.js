const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')    
const connection = mysql.createConnection({
    host: 'localhost',
    user:'root',
    password:'root',
    database:'RestaurantSystem'
}); 

connection.connect();
const express = require('express');
const app = express()
const port = 4000

/* Middleware for Authenticating User Token */
function authenticeteToken(req,res,next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err,user) =>{
        if (err) { return res.sendStatus(403) }
        else{
            req.user = user
            next()
        }
    })
}

/* API for Registering to a new Order*/
app.post("/register_orderlist",authenticeteToken, (req, res) => {
   
    let customer_id = req.query.customer_id
    let menu_id = req.query.menu_id
    let orderlist_name = req.query.orderlist_name
    let orderlist_location = req.query.orderlist_location
    
    let query = `INSERT INTO Orderlist
                    (CustomerID, MenuID, OrderlistName, OrderlistLocation)
                    VALUES ('${customer_id}',
                            '${menu_id}',
                            '${orderlist_name}',
                            '${orderlist_location}'
                            )`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Adding orderlist succesful"
              })
        }
    })
})

/*API for Processing Customer Authorization*/
app.post("/login", (req, res) =>{
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Customer WHERE Username = '${username}'`
    connection.query( query, (err,rows) =>{
                if (err) {
                    console.log(err)
                    res.json({
                        "status" : "400",
                        "message" : "Error querying from customer db"
                        })
                }else {
                    let db_password = rows[0].Password
                    bcrypt.compare(user_password, db_password, (err, result) =>{
                        if (result){
                           let payload = {
                               "username" : rows[0].Username,
                               "user_id" : rows[0].CustomerID,
                           }

                           let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d' } )
                           res.send(token)
                        }else { res.send ("Invalid username / password")}
                    })
                    
                } 
    })
})

/*API for Registering a new Customer*/
app.post("/add_customer", (req, res) => {
    
    let customer_name = req.query.customer_name
    let customer_surname = req.query.customer_surname
    let customer_number = req.query.customer_number
    let customer_username = req.query.customer_username
    let customer_password = req.query.customer_password
    
    bcrypt.hash(customer_password, SALT_ROUNDS, (err,hash) => {

        let query = `INSERT INTO Customer
                    (CustomerName, CustomerSurname,CustomerNumber, Username, Password)
                    VALUES ('${customer_name}','${customer_surname}','${customer_number}',
                            '${customer_username}','${hash}'
                    )`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Adding new user succesful"
              })
        }
    })
  })

})

/* CRUD Operation for  Menu Table*/
app.get("/list_menu",(req,res) =>{
    let query = "SELECT * FROM  Menu";
    connection.query( query,(err, rows) =>{
        if (err) {
            res.json({
                   "status" : "400",
                   "message" : "Error querying from menu db"
                 })
        }else {
            res.json(rows)
        }
    });

})

app.post("/add_menu",(req,res) =>{
    let menu_name  = req.query.menu_name
    let menu_price = req.query.menu_price
    let menu_status = req.query.menu_status
    let query = `INSERT INTO  Menu 
                         (MenuName, MenuPrice, MenuStatus)
                            VALUES ('${menu_name}','${menu_price}','${menu_status}')`
    console.log(query)

    connection.query( query,(err, rows) =>{
        if (err) {
            console.log(err)
            res.json({
                   "status" : "400",
                   "message" : "Error inserting data into db"
            })
        }else {
            res.json ({
                    "status" : "200",
                    "message" : "Adding menu succesful"
            })
        }
    });
})

app.post("/update_customer", (req, res) => {
    let customer_id =  req.query.customer_id
    let customer_name = req.query.customer_name
    let customer_surname = req.query.customer_surname
    let customer_number = req.query.customer_number
    
    let query = `UPDATE Customer SET
                    CustomerName ='${customer_name}',
                    CustomerSurname ='${customer_surname}',
                    CustomerNumber ='${customer_number}'
                    WHERE CustomerID =${customer_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error updating record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Updating customer succesful"
              })
        }
    })
})

app.post("/update_menu", (req, res) => {
    let menu_id = req.query.menu_id
    let menu_name = req.query.menu_name
    let menu_price = req.query.menu_price
    let menu_status =  req.query.menu_status

    let query = `UPDATE Menu SET
                    MenuName ='${menu_name}',
                    MenuPrice ='${menu_price}',
                    MenuStatus ='${menu_status}'
                    WHERE MenuID =${menu_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error updating record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Updating menu succesful"
              })
        }
    })
})

app.post("/delete_customer",(req,res) =>{
    let customer_id = req.query.customer_id

    let query = `DELETE FROM Customer WHERE CustomerID = ${customer_id}`
                    
    console.log(query)

    connection.query( query,(err, rows) =>{
        if (err) {
            console.log(err)
            res.json({
                   "status" : "400",
                   "message" : "Error deleting record"
            })
        }else {
            res.json ({
                    "status" : "200",
                    "message" : "Deleting record success"
            })
        }
    });
})

app.post("/delete_menu",(req,res) =>{
    let menu_id = req.query.menu_id

    let query = `DELETE FROM Menu WHERE MenuID = ${menu_id}`
                    
    console.log(query)

    connection.query( query,(err, rows) =>{
        if (err) {
            console.log(err)
            res.json({
                   "status" : "400",
                   "message" : "Error deleting record"
            })
        }else {
            res.json ({
                    "status" : "200",
                    "message" : "Deleting record success"
            })
        }
    });
})

app.listen(port,() =>{
    console.log( `Now starting Restaurant System Backend at port ${port}` )
})
