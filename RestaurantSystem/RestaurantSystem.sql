-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 04, 2022 at 04:08 AM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `RestaurantSystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `CustomerID` int(11) NOT NULL,
  `CustomerName` varchar(200) NOT NULL,
  `CustomerSurname` varchar(200) NOT NULL,
  `CustomerNumber` int(100) NOT NULL,
  `Username` varchar(200) NOT NULL,
  `Password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Customer`
--

INSERT INTO `Customer` (`CustomerID`, `CustomerName`, `CustomerSurname`, `CustomerNumber`, `Username`, `Password`) VALUES
(1, 'manita', 'kaewpi', 654325678, '0', '0'),
(2, 'pimrapat', 'padung', 977891234, '0', '0'),
(5, 'milin', 'dokthian', 876615454, '0', '0'),
(6, 'jiradapa', 'intajak', 624475151, 'jiradapa', '$2b$10$pfQt5IartTAkBb5DbxWeYOfhV/mL4gX4TISjaGcoCwIPc8PJu4B7y'),
(8, 'punsikorn', 'tiyakorn', 653345643, 'punsikorn', '$2b$10$j2tBfSRgk2CV2hi4WxBDdeLHAWKVbcrdx.zC7BkTb92ySlbWDEbHu'),
(9, 'paichanan', 'jiajirachote', 937786754, 'paichanan', '$2b$10$4iDQxDkGNK.Y/YybUwO18u.3ljb4P1mpu9YOBorkn5RVBxt3uZbiC'),
(11, 'jannis', 'oprasert', 987654321, 'jannis', '$2b$10$VohnZzEj.fDF6Cl4t63YJuw0a8bsgFxix5F1uzNVJjOmBs0hBV9Gm');

-- --------------------------------------------------------

--
-- Table structure for table `Menu`
--

CREATE TABLE `Menu` (
  `MenuID` int(11) NOT NULL,
  `MenuName` varchar(200) NOT NULL,
  `MenuPrice` int(200) NOT NULL,
  `MenuStatus` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Menu`
--

INSERT INTO `Menu` (`MenuID`, `MenuName`, `MenuPrice`, `MenuStatus`) VALUES
(1, 'pizza', 399, 'มี'),
(2, 'steak', 100, 'หมดแล้ว'),
(3, 'lasagna', 150, 'มี'),
(4, 'french frice', 30, 'หมดแล้ว'),
(9, 'burger', 85, 'หมดแล้ว'),
(10, 'papaya salad', 100, 'มี'),
(11, 'spaghetti', 100, 'มี'),
(12, 'lasagya', 150, 'มี'),
(13, 'pasta', 100, 'มี');

-- --------------------------------------------------------

--
-- Table structure for table `Orderlist`
--

CREATE TABLE `Orderlist` (
  `OrderlistID` int(11) NOT NULL,
  `OrderlistName` varchar(200) NOT NULL,
  `OrderlistLocation` varchar(200) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `MenuID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Orderlist`
--

INSERT INTO `Orderlist` (`OrderlistID`, `OrderlistName`, `OrderlistLocation`, `CustomerID`, `MenuID`) VALUES
(1, 'papaya salad', 'หาดใหญ่', 2, 10),
(2, 'papaya salad', 'หาดใหญ่', 2, 10),
(3, 'spaghetti', 'ควนโส', 5, 11),
(4, 'pizza', 'กรุงเทพ', 11, 1),
(5, 'burger', 'ม.อ.หาดใหญ่', 9, 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`CustomerID`);

--
-- Indexes for table `Menu`
--
ALTER TABLE `Menu`
  ADD PRIMARY KEY (`MenuID`);

--
-- Indexes for table `Orderlist`
--
ALTER TABLE `Orderlist`
  ADD PRIMARY KEY (`OrderlistID`),
  ADD KEY `CustomerID` (`CustomerID`),
  ADD KEY `MenuID` (`MenuID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Customer`
--
ALTER TABLE `Customer`
  MODIFY `CustomerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `Menu`
--
ALTER TABLE `Menu`
  MODIFY `MenuID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `Orderlist`
--
ALTER TABLE `Orderlist`
  MODIFY `OrderlistID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Orderlist`
--
ALTER TABLE `Orderlist`
  ADD CONSTRAINT `CustomerID` FOREIGN KEY (`CustomerID`) REFERENCES `Customer` (`CustomerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `MenuID` FOREIGN KEY (`MenuID`) REFERENCES `Menu` (`MenuID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
